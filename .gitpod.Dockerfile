FROM gitpod/workspace-full

RUN sudo DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:cncf-buildpacks/pack-cli && \
  sudo DEBIAN_FRONTEND=noninteractive apt-get update && \
  sudo DEBIAN_FRONTEND=noninteractive apt-get install pack-cli -y && \
  sudo rm -rf /var/lib/apt/lists/*